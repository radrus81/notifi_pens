import React, { Component } from 'react'
import { Container, Row, Col } from 'reactstrap';
import Header from './Header/Header'
import Authorization from './Authorization/Authorization'
import Footer from './Footer/Footer'

class Layout extends Component {
    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <Header />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <main>
                            <Authorization />
                        </main>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Footer />
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default Layout