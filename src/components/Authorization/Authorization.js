import React, { Component } from 'react'
import { Formik, Form, Field } from 'formik'
import AuthorizationFormSchema from './AuthorizationFormSchema'
import './Authorization.scss'
import login from '../../img/login.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faKey } from '@fortawesome/free-solid-svg-icons'
import axios from 'axios'



class Authorization extends Component {

    render() {
        return (
            <section>
                <div className="Authorization wrap">
                    <img src={login} alt="Scanfcode" />
                    <h1>Введите свои данные</h1>
                    <Formik
                        initialValues={{
                            login: '',
                            pass: ''
                        }}
                        validationSchema={AuthorizationFormSchema}
                        onSubmit={async (values) => {
                            var data = new FormData();
                            data.append('login', values.login)
                            data.append('pass', values.pass)
                            axios({
                                url: 'http://localhost:8080/notifi_pens/Authorization.php',
                                method: 'POST',
                                responseType: 'json',
                                data: data
                            })
                                .then(response => {
                                    console.log(response.data)
                                })
                                .catch(error => {

                                })
                        }}
                        render={({ errors, touched, values, isSubmitting }) => (
                            < Form >
                                {
                                    errors.login && touched.login &&
                                    <div className="messErr alert-danger">{errors.login}</div>
                                }
                                <div className="iconInput">
                                    <FontAwesomeIcon icon={faUser} size="lg" />
                                    <Field
                                        name="login"
                                        placeholder="Введите логин"
                                        type="text"
                                        value={values.login || ''}
                                    />
                                </div>
                                {
                                    errors.pass && touched.pass &&
                                    <div className="messErr alert-danger">{errors.pass}</div>
                                }
                                <div className="iconInput">

                                    <FontAwesomeIcon icon={faKey} size="lg" />
                                    <Field
                                        name="pass"
                                        placeholder="Введите пароль"
                                        type="password"
                                        value={values.pass || ''}
                                    />
                                </div>
                                <button id="send" className="btn-primary" type="submit" disabled={isSubmitting}>Войти</button>
                            </Form>
                        )}
                    />
                </div>
            </section>
        )
    }
}

export default Authorization 