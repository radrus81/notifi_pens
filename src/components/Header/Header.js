import React from 'react'
import './Header.scss'


const Header = () => {
    return (
        <header>
            <div className="header wrap">
                <h1>Сервис уведомления пенсионеров</h1>
            </div>
        </header>
    )
}

export default Header 